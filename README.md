# React Dropdown Component

A simple and customizable dropdown component created using React.

## Key Functionalities:

- Dropdown Button: Hover over the button to display the dropdown items.
- Items List: The dropdown component receives an array of items to display in the list.
- Item Selection: Clicking on an option in the dropdown list will close the dropdown.
- Customizable Styling: Style the dropdown component as you like.


## Technologies Employed:

- React: A JavaScript library for building user interfaces.
- CSS: Facilitates visually pleasing and responsive design.


## Requirements

For development, you will only need Node.js (16+), a node global package (Npm), and mongoDB atlas URI or locally installed mongodb server.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

  If the installation was successful, you should be able to run the following command.

    $ node --version <br>
    V16.xx.xx

    $ npm --version <br>
    x.xx.xx

  If you need to update `npm`, you can make it using `npm` !Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g


## Install Project

    $ git clone <Your-Repository-URL>
    $ cd dropdown-component
    $ npm install

## Running the project on development
    # Set up .env file in the root, format is given in the .env.example
    
    $ npm start


## Folder Structure
```
[React Dropdown Component]
│   .gitignore
│   package-lock.json
│   package.json
│   README.md
│
├───public
│       index.html
│       ...
│
├───src
│   │   App.js
│   │   index.js
│   │   Dropdown.js
│   │   Dropdown.css
│   │   ...
│
└───node_modules
        ...
```


## Support

Contributions, bug reports, and feature requests are welcome. Feel free to fork the repository and submit pull requests to contribute to the project.