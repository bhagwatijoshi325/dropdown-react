import React from 'react';
import Dropdown from './Dropdown';
import './Dropdown.css'; 
import './App.css';

function App() {
  const items = ['Select', 'Yes', 'Probably Not'];

  return (
    <div className="App">
      <h1>Dropdown Component</h1>
      <Dropdown items={items} />
    </div>
  );
}

export default App;
