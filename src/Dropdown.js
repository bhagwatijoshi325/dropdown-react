import React, { useState } from 'react';
const Dropdown = ({ items }) => {
    const [isOpen, setIsOpen] = useState(false);
  
    const handleMouseEnter = () => {
      setIsOpen(true);
    };
  
    const handleMouseLeave = () => {
      setIsOpen(false);
    };
  
    const handleItemClick = () => {
      setIsOpen(false);
    };
  
    return (
      <div className="dropdown" onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
        <p> Should You use a drop down.</p>
        <button className="dropdown-button">Hover me </button>
        {isOpen && (
          <ul className="dropdown-list">
            {items.map((item, index) => (
              <li key={index} className="dropdown-item" onClick={handleItemClick}>
                {item}
              </li>
            ))}
          </ul>
        )}
      </div>
    );
  };
  
  export default Dropdown;
  